#!/bin/bash
set -e

# Git - Variables
# NOTE: GIT_REPOSITORY_URL should include basic credentials (eg. https://username:password-or-api-key@bitbucket.org/organization/project.git)
WWW_ROOT=${WWW_ROOT}
GIT_REPOSITORY_URL=${GIT_REPOSITORY_URL}
GIT_REPOSITORY_BRANCH=${GIT_REPOSITORY_BRANCH}

# Git - Retrieving source
cd $WWW_ROOT
rm -rf $WWW_ROOT/*
export GIT_TRACE_PACKET=1
export GIT_TRACE=1
export GIT_CURL_VERBOSE=1
git config --global http.postBuffer 157286400
git clone -b $GIT_REPOSITORY_BRANCH $GIT_REPOSITORY_URL $WWW_ROOT

# Codeigniter - Variables
CODEIGNITER_DIR=${CODEIGNITER_DIR}

# Codeigniter - Directory Permissions
chgrp -R www-data "$WWW_ROOT""$CODEIGNITER_DIR"/application/cache
chmod -R ug+rwx "$WWW_ROOT""$CODEIGNITER_DIR"/application/cache
chgrp -R www-data "$WWW_ROOT""$CODEIGNITER_DIR"/application/logs
chmod -R ug+rwx "$WWW_ROOT""$CODEIGNITER_DIR"/application/logs